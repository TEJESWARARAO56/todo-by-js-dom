let todosList = document.getElementById("todolist");
let inputEl = document.getElementById("inputid");
let toggleIcon = document.getElementById("toggleAll");
let activeFilter = localStorage.getItem('activefilter') || "All";
let existedTodos = JSON.parse(localStorage.getItem("todos")) || []

function showtoggleAll() {
    if (existedTodos.length > 0) {
        document.getElementById('filter').classList.remove("displayfilter")

        document.getElementById("toggleAll").classList.remove("display")
        if (existedTodos.some((todo) => !todo.isChecked)) {
            document.getElementById("toggleAll").classList.remove("icondarkmode")
        } else {
            document.getElementById("toggleAll").classList.add("icondarkmode")
        }
    } else {
        document.getElementById('filter').classList.add("displayfilter")
        document.getElementById("toggleAll").classList.add("display")
    }
}

toggleIcon.addEventListener(("click"), (event) => {
    let everytodochecked = document.getElementById("toggleAll").className.includes("icondarkmode")
    existedTodos = existedTodos.map((todo) => {
        todo.isChecked = everytodochecked ? false : true
        return todo
    })
    renderAllTodos(existedTodos)
    localStorage.setItem("todos", JSON.stringify(existedTodos))
    showtoggleAll();
    countTodosLeft();
    toggleClearCompleted();
})

document.getElementById("toggleAll").addEventListener("click", (event) => {
    console.log(event.target)
    if (event.target.value === "true") {
        console.log("gi")
        existedTodos = existedTodos.map((todo) => {
            todo.isChecked = todo.isChecked
            return todo
        })
        return todo
    }
})

function toggleClearCompleted() {
    if (existedTodos.findIndex((todo) => {
        return todo.isChecked
    }) === -1) {
        document.getElementById("clearCompleted").classList.add("display")
    } else {
        document.getElementById("clearCompleted").classList.remove("display")
    }
}

inputEl.addEventListener('keydown', (event) => {
    if (event.key === 'Enter' && event.target.value !== "") {
        let newObj = {
            todoText: event.target.value,
            isChecked: false,
            id: new Date().getTime().toString()
        }
        console.log(newObj)
        existedTodos.push(newObj);
        showtoggleAll()
        localStorage.setItem("todos", JSON.stringify(existedTodos))
        renderTodoItem(newObj);
        event.target.value = ""
        countTodosLeft();
    }
})

function onChecking(event) {
    let id = event.target.parentElement.parentElement.id
    let indexOfTargetTodo = existedTodos.findIndex((todo) => {
        if (todo.id === id) {
            return true
        }
    })
    existedTodos[indexOfTargetTodo].isChecked = !existedTodos[indexOfTargetTodo].isChecked
    todosList.textContent = ""
    localStorage.setItem('todos', JSON.stringify(existedTodos));
    renderAllTodos(existedTodos)
    countTodosLeft()
    toggleClearCompleted()
    showtoggleAll();
    onApplyingFilter();
}


function removeItem(event) {
    let parentEl = event.target.parentElement
    let id = event.target.parentElement.id
    existedTodos = existedTodos.filter((todo) => {
        return todo.id !== id
    });
    localStorage.setItem("todos", JSON.stringify(existedTodos))
    todosList.removeChild(parentEl);
    countTodosLeft()
    showtoggleAll()
    toggleClearCompleted()
}

function renderTodoItem(todo) {
    let todoli = document.createElement("li")
    todoli.className = "todo-item"
    todoli.setAttribute("id", todo["id"])
    todosList.append(todoli)
    let checkboxContainer = document.createElement("div");
    checkboxContainer.className = "todo-text-and-checkbox";
    todoli.appendChild(checkboxContainer)

    let checkbox = document.createElement("input");
    checkbox.type = "checkbox"
    checkbox.className = "input"
    let checkBoxid = "checkbox" + todo["id"]
    checkbox.setAttribute("id", checkBoxid)
    checkbox.style.display = "none"
    checkboxContainer.appendChild(checkbox)
    checkbox.checked = todo.isChecked

    checkbox.addEventListener('click', (event) => {
        console.log("kop")
        onChecking(event)
    })

    let labelEl = document.createElement("label");
    labelEl.className = "checkbox-container"
    labelEl.setAttribute("for", checkBoxid)
    checkboxContainer.appendChild(labelEl);

    let tickIcon = document.createElement("i");
    tickIcon.className = "fa-sharp fa-solid fa-circle-check circlecheck"
    labelEl.appendChild(tickIcon);

    let todotextEl = document.createElement("p");
    todotextEl.className = "todotext"
    todotextEl.textContent = todo.todoText;
    checkboxContainer.appendChild(todotextEl);


    todotextEl.addEventListener('click', (event) => {
        onChecking(event)
    })

    todotextEl.addEventListener(("dblclick"), (event) => {
        event.target.setAttribute("outline", "none");
        event.target.setAttribute("contenteditable", "true");
    })

    todotextEl.addEventListener("keydown", (event) => {
        if (event.key === "Enter") {
            event.target.setAttribute("contenteditable", "false");
            let targetTodo = event.target.parentElement.parentElement
            let indexOftargetEl = existedTodos.findIndex((todo) => {
                return todo.id === targetTodo.id
            })
            console.log(indexOftargetEl)
            existedTodos[indexOftargetEl]['todoText'] = event.target.textContent
            localStorage.setItem('todos', JSON.stringify(existedTodos))
            renderAllTodos(existedTodos);
        }
    })

    let cancelIcon = document.createElement("i");
    cancelIcon.className = "fa-solid fa-xmark cancelicon";
    todoli.appendChild(cancelIcon);

    cancelIcon.addEventListener("click", (event) => {
        removeItem(event)
    })
}

function renderAllTodos(existedTodos) {
    todosList.textContent = ""
    existedTodos.forEach((todo) => {
        renderTodoItem(todo)
    })
}

function onApplyingFilter(event) {
    if (event) {
        activeFilter = event.target.id
        localStorage.setItem('activefilter', activeFilter)
        let childElements = event.target.parentElement.children;
        Array.from(childElements).forEach((element) => {
            element.classList.remove("active-filter")
        })
        event.target.classList.add("active-filter");
        if (event.target.textContent === "All") {
            renderAllTodos(existedTodos)
        } else if (event.target.textContent === "Active") {
            renderAllTodos(existedTodos.filter((todo) => !todo.isChecked))
        }
        else {
            renderAllTodos(existedTodos.filter((todo) => todo.isChecked))
        }
    } else {
        activeFilter = localStorage.getItem('activefilter')
        let childElements = document.getElementById('filters').children;
        Array.from(childElements).forEach((element) => {
            element.classList.remove("active-filter")
        })
        document.getElementById(activeFilter).classList.add("active-filter");
        if (activeFilter === "All") {
            renderAllTodos(existedTodos)
        } else if (activeFilter === "Active") {
            renderAllTodos(existedTodos.filter((todo) => !todo.isChecked))
        }
        else {
            renderAllTodos(existedTodos.filter((todo) => todo.isChecked))
        }

    }

}

function onClearCompleting() {
    existedTodos = existedTodos.filter((todo) => !todo.isChecked)
    localStorage.setItem("todos", JSON.stringify(existedTodos));
    renderAllTodos(existedTodos);
    showtoggleAll()
    toggleClearCompleted()
    countTodosLeft()
}

function countTodosLeft() {
    document.getElementById("itemsLeft").textContent = existedTodos.reduce((acc, element) => {
        if (element.isChecked === true) {
            return acc
        }
        return acc + 1
    }, 0) + " "
}

countTodosLeft()
showtoggleAll()
toggleClearCompleted()
renderAllTodos(existedTodos)
onApplyingFilter()

function print(){
    console.log("hi")
}
function one(){
    console.log("one")
}

function two(){
    console.log("two")
}

function three(){
    console.log("three")
}
function four(){
    console.log("four")
}
function first(){
    console.log("first")
}